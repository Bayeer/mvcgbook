<?php
require_once 'configs/conf.php';
require_once 'includes/autoload.php';

$q = Guard::get('act');
$controller = new Controller($q);
$controller->processRequest($q);
