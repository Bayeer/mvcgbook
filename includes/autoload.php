<?php

function __autoload($class)
{
    static $files= array(
        'Guard'                    => 'classes/Guard.class.php',
        'Model'                    => 'classes/Model.class.php',
        'View'                     => 'classes/View.class.php',
        'Controller'               => 'classes/Controller.class.php',
        'Message'                  => 'classes/DAL/Message.php',
    
        'PropertyObject'           => 'classes/PropertyObject.class.php',
     );
    if(!array_key_exists($class, $files)) return;
    require_once($files[$class]);
}
