<?php
/**
 * PropertyObject declaration 
 */
/**
 * Базовый класс для объектов, вытягиваемых из бд.
 * 
 * Фактически все поля хранятся в массиве $this->mData
 */
abstract class PropertyObject {
	/**
	 * Массив, хранящий все поля для объекта, вытягиваемого из бд
	 * @var array
	 */
    protected $mData = array();
    /**
     * Пустой конструктор
     */
    public function __construct() {}

    public function __set($name, $value)
    {
    	//echo '<div>__set</div>';
        $this->mData[$name] = $value;
    }

    public function __get($name)
    {
        //writeln("__get($name)");
        //if (isset($this->$name))
        if (array_key_exists($name, $this->mData))
            return $this->mData[$name];
//        else
            //throw new Exception('no such property');
    }

    public function __isset($name)
    {
        //writeln("__isset($name)");
        return isset($this->mData[$name]);
    }

    public function __unset($name)
    {
        unset($this->mData[$name]);
    }
}
?>