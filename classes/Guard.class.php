<?php
class Guard
{
	public static function get($key)
	{
		if (array_key_exists($key, $_GET))
			return @$_GET[$key];
		return null;
	}
	public static function post($key)
	{
		if (array_key_exists($key, $_POST))
		      return @$_POST[$key];
		return null;
	}
}