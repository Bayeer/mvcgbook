<?php  n
class Controller
{
	private $V;
	private $M;
	
	public function __construct()
	{
        $this->V = new View();
        $this->M = new Model();
	}
    
    public function processRequest($query) {
        switch ($query)
		{
			case 'add':
				$title = Guard::post('txtTitle');
				$author = Guard::post('txtAuthor');
				$content = Guard::post('txtContent');
				if (!$title && !$content) // Не постились ли поля формы нового сообщения
				{
					$this->V->showNewMessage();
				}
				else
				{
					if (!$this->M->addMessage($title, $content, $author))
					   //$this->V->showError("Cannot add message");
					   $this->V->showError();
					else
					{
						$this->redirect('index.php');
					}
				}
				break;
			case 'edit':
				$id = Guard::post('id');
				
				if ($id > 0)
			    {
		            $title = Guard::post('txtTitle');
	                $author = Guard::post('txtAuthor');
	                $content = Guard::post('txtContent');
		            if (!$this->M->updateMessage($id, $title, $content, $author))
		                $this->V->showError("Ошибка при редактировании записи");
		            else
		            {
		                $this->redirect('index.php');
		            }
			    }
			    else
			    {
			    	$id = Guard::get('id');
			    	$message = $this->M->getMessage($id);
                    if (!$message)
                        $this->V->showError("Message does not exists");
                    else
                        $this->V->showEditForm($message);
			    }
				break;
			case 'delete':
				$id = Guard::get('id');
				if ($id > 0)
				{
					if (!$this->M->deleteMessage($id))
					    $this->V->showError("Cannot delete message");
				    else
				        $this->redirect('index.php');
					   
				}
				break;
			case '':
			default:
				$messages = $this->M->getMessages();
				$this->V->showIndex($messages);
				break;
		}
    }
    
	public function redirect($url)
	{
	    $wait = 1;
        header($_SERVER['SERVER_PROTOCOL']." 303 See Other");
        header("Location: ".$url);
        header("Content-type: text/html; charset=UTF-8");
        $hUrl = htmlspecialchars($url);
echo <<<PAGE
<!DOCTYPE html>
<meta http-equiv="Content-type" content="text/html;charset=utf-8">
<head>
<title>Переадресация</title>
<script type="text/javascript">function doRedirect(){location.replace({$hUrl});}</script>
<style type="text/css">p{font-family:Arial, sans-serif}</style>
</head>
<body onload="doRedirect()">
<noscript><meta http-equiv="refresh" content="{$wait}; url='{$hUrl}'"></noscript>
<p>Подождите...</p>
<p>Если переадресация не сработала, перейдите по <a href="{$hUrl}">ссылке</a> вручную.</p>
</body>
PAGE;
        die();
	}
}