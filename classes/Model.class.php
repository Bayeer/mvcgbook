<?php
class Model
{
	private $db;
	
	public function __construct()
	{
		$hostname = MYSQL_DBHOST;
		$dbname = MYSQL_DBNAME;
		$username = MYSQL_DBUSER;
		$password = MYSQL_DBPASS;
        $connection_string = sprintf("mysql:host=%s;dbname=%s", $hostname, $dbname);
        $this->db = new PDO($connection_string, $username, $password);
        $this->db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        $this->db->exec('SET NAMES utf8');
	}
	public function addMessage($title, $content, $author)
	{
		$query = "INSERT INTO messages VALUES (0, :title, NOW(), :content, :author)";
		$st = $this->db->prepare($query);
		$st->bindValue(':title', $title, PDO::PARAM_STR);
		$st->bindValue(':content', $content, PDO::PARAM_STR);
		$st->bindValue(':author', $author, PDO::PARAM_STR);
		$st->execute();
		return true;
	}
	public function deleteMessage($id)
	{
		$query = 'DELETE FROM messages WHERE id = :id';
		$st = $this->db->prepare($query);
		$st->bindValue(':id', $id, PDO::PARAM_INT);
		if ($st->execute() > 0)
		    return true;
		return false; 
	}
	
	public function updateMessage($id, $title, $content)
	{
		$query = "UPDATE messages SET title = '$title', content = '$content' WHERE id=$id";
        $n = $this->db->exec($query);
        if ($n > 0)
            return true;
        return false;
	}
	public function getMessage($id)
	{
		$query = 'SELECT * FROM messages WHERE id=:id';
		$st = $this->db->prepare($query);
		$st->bindValue(':id', $id, PDO::PARAM_INT);
		$st->execute();
		$message = $st->fetchObject('Message');
		if (!$message)
		    return null;
		return $message;
	}
	public function getMessages($where = array())
	{
		$query = 'SELECT * FROM messages';
		$st = $this->db->prepare($query);
		$st->execute();
		
		$messages = array();
		while ($o = $st->fetchObject('Message'))
		{
			$messages[] = $o;
		}
		return $messages;
	}
}