<?php
class View
{
	public function __construct()
	{
	}
	///////////////////////
	public function showIndex($messages = null)
	{
		$this->printHeader();
		$this->printNavigation();
		$this->printMessages($messages);
		$this->printFooter();
	}
	public function showNewMessage()
	{
		$this->printHeader();
		$this->printNavigation();
		$this->printNewMessageForm();
		$this->printFooter();
	}
	
    public function showEditForm($message)
    {
        $this->printHeader();
        $this->printNavigation();
        $this->printEditMessageForm($message);
        $this->printFooter();
    }
	
	public function showError($err_msg = 'no error', $err_code = 0)
	{
		$this->printHeader();
		$this->printNavigation();
		$this->printError($err_msg, $err_code);
		$this->printFooter();
	}
	//----------------------------------------------------------------------
	public function printHeader()
	{
		include 'tpl/header.php';
	}
	public function printFooter()
	{
		include 'tpl/footer.php';
	}
	public function printNavigation()
	{
		include 'tpl/navigation.php';
	}
	public function printMessages($messages)
	{
		include 'tpl/messages.php';
	}
	public function printNewMessageForm()
	{
		include 'tpl/form.php';
	}
	
    public function printEditMessageForm($message)
    {
        include 'tpl/editform.php';
    }
	
	public function printError($err_msg, $err_code)
	{
	    include 'tpl/error.php';
	}
}